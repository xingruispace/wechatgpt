# wechatbot
> 最近chatGPT异常火爆，提供多种方式访问
> 方式一：访问 https://openai.com/ 通过网页使用
> 方式二：通过后台接口调用，需要注册账号后开通接口访问权限
> 方式三：通过接口接入到不同的前段，比如微信小程序、微信等访问

> 本项目可以将个人微信化身GPT机器人. 大家可以添加微信 evolove007 或者关注公众号 星瑞元宇 发送chatgpt体验效果
> 项目基于[wechatbot](原作者停止维护，已获得作者许可) 开发。
> 项目包含引用[openwechat](https://github.com/eatmoreapple/openwechat) 开发。


### 目前实现了以下功能
 * 提问增加上下文，更接近官网效果 
 * 机器人群聊@回复
 * 机器人私聊回复
 * 好友添加自动通过
 * chatgpt收费后，增加调用次数的控制

 
# 使用前提
> *  有openai账号，并且创建好api_key，注册事项可以参考[此文章](https://juejin.cn/post/7173447848292253704) 。
> * 微信必须实名认证。
> * 直接使用打包好的可执行程序
> * 或者安装go语言后自行编译
> * 重启后，删除storage.json

# 注意事项
> * 项目仅供娱乐，滥用可能有微信封禁的风险，请勿用于商业用途。
> * 请注意收发敏感信息，本项目不做信息过滤。

# 快速开始
> 非技术人员请直接下载/bin目录下的(wechatbot-amd64.exe)，修改config.json文件后，运行exe。

> go语言基础的技术人员按照下面的方法使用：
````
# 获取项目
git clone https://gitee.com/xingruispace/wechatgpt.git

# 进入项目目录
cd wechatbot

# 复制配置文件
copy config.dev.json config.json
修改config.json 中的key为个人的key

# 启动项目
go run main.go
````

# 配置文件说明
````
{
"api_key": "your api key",
"auto_pass": true,
"session_timeout": 60
}

api_key：openai api_key
auto_pass:是否自动通过好友添加
session_timeout：会话超时时间，默认60秒，单位秒，在会话时间内所有发送给机器人的信息会作为上下文。
````

# 使用示例
### 向机器人发送`我要问下一个问题`，清空会话信息。
### 私聊


### 群聊@回复


### 添加微信（备注: wechatgpt）进群交流

**如果二维码图片没显示出来，请添加微信号 evolove007**



